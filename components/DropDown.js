import React from "react";
import { View, TextInput } from "react-native";
import ModalSelector from "react-native-modal-selector";
import { Container,Left, Right } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
import { Tooltip, Text } from "react-native-elements";

const DropDown = (props) => {
  return (
    <>
      <View
        style={{
          flex: 1,
          justifyContent: "space-around",
          marginBottom: 10,
          backgroundColor: "#e5e6e4",
          // borderWidth: 1,
          borderColor: "#212529",
          borderRadius: 10,
       
        }}
      >
        <ModalSelector
          data={props.data}
          onChange={props.onChange}
          overlayStyle={{
            flex: 1,
            padding: "10%",
            justifyContent: "center",
            backgroundColor: "rgba(0,0,0,0.8)",
          }}
          optionTextStyle={{ color: "black", fontSize: 18 }}
          cancelTextStyle={{
            color: "black",
            fontSize: 18,
            textTransform: "uppercase",
            fontWeight: "700",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              borderWidth: 1,
              borderColor: "#212529",
              borderRadius: 10,
              padding: 5,
            }}
          >
            <TextInput
              style={{ flex: 1, fontSize: 16 }}
              placeholderTextColor="black"
              editable={false}
              value={props.value}
              placeholder={props.placeholder}
            />
            <MaterialIcons
              name="keyboard-arrow-down"
              style={{ color: "black" }}
              size={24}
            />
          </View>
        </ModalSelector>
     
      </View>
     
    </>
  );
};

export default DropDown;
