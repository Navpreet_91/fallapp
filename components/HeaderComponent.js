import React from "react";
import { withNavigation } from "react-navigation";
import {
  Left,
  Right,
  Button,
  Icon,
  Title,
  Header,
  Body,
  Subtitle,
} from "native-base";
import { MAIN_THEME_COLOR_1, WHITE } from "../constants/Colors";
import styled from "styled-components";
import { DrawerActions } from "react-navigation-drawer";
import { MaterialIcons } from "@expo/vector-icons";

export const StyledHeader = styled(Header)`
  background-color: ${MAIN_THEME_COLOR_1};
`;

export const StyledHeaderBody = styled(Body)`
  flex: 1;
  align-self: center;
  align-items: flex-start;
  margin-left: 15px;
`;

const HeaderComponent = (props) => {
  return (
    <StyledHeader>
      <Left style={{ flex: 0.5 }}>
        {props.backRoute && (
          <Button
            transparent
            onPress={() => {
              if (props.backRoute == "back") {
                props.navigation.goBack();
              } else {
                props.navigation.push(`${props.backRoute}`);
              }
            }}
          >
            <MaterialIcons
              name="keyboard-arrow-left"
              style={{ color: WHITE,margin:0 }}
              size={34}
            />
  
          </Button>
        )}
      </Left>
      <StyledHeaderBody style={{ flex: 1 }}>
        <Title style={{ color: WHITE }}>{props.title}</Title>
  
      </StyledHeaderBody>

    </StyledHeader>
  );
};
export default withNavigation(HeaderComponent);
