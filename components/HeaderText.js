import React from 'react';
import { View, Text } from 'react-native';

const HeaderText = (props) => {
  return (
    <View
      style={{
        padding: 5,
        marginTop: 10
      }}
    >
      <Text style={{ fontSize: 18, fontWeight: 'bold', ...props.textStyle }}>{props.title}</Text>
      <Text style={{ fontSize: 16, color: '#afafaf' }}>{props.subtitle}</Text>

    </View>
  );
};

export default HeaderText;
