import React from 'react';

import { withNavigation } from 'react-navigation';
import { RefreshControl, View, Text, ActivityIndicator } from 'react-native';
import { Content, Container } from 'native-base';

//Components
import HeaderComponent from './HeaderComponent';

//Styles
import { MAIN_BACKGROUND_LIGHT } from '../constants/Colors';
const DrawerWrapper = props => {
  return (
    <Container style={{ flex: 1 }}>
      <HeaderComponent
        title={props.title}
        subtitle={props.subtitle}
        backRoute={props.backRoute}
        hasTabs={props.hasTabs}
      />

      {!props.tabs ? (
        props.localLoading ? (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <ActivityIndicator />
            <Text> Loading </Text>
          </View>
        ) : props.noScroll ? (
          <View style={{ flex: 1, ...props.styles }}>{props.children}</View>
        ) : (
          <Content
            style={{
              ...props.styles,
              backgroundColor: MAIN_BACKGROUND_LIGHT
            }}
            padder={props.padder}
            refreshControl={
              props.handleRefresh && (
                <RefreshControl
                  refreshing={false}
                  onRefresh={props.handleRefresh}
                />
              )
            }
          >
            {props.children}
          </Content>
        )
      ) : (
        props.tabs()
      )}
    </Container>
  );
};

export default withNavigation(DrawerWrapper);
