import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import AppLoading from 'expo-app-loading'
import * as Font from 'expo-font';
import * as Icon from '@expo/vector-icons';
import { PersistGate } from 'redux-persist/integration/react';
import { Root } from 'native-base';
import { ActionSheetProvider } from '@expo/react-native-action-sheet';
import AppNavigator from '../fallapp/navigation/AppNavigator';
import { withAuthenticator } from 'aws-amplify-react-native';
import Amplify from 'aws-amplify';
import awsmobile from '../fallapp/src/aws-exports';

//redux
import { Provider } from 'react-redux';
// import { store, persistor } from './store';
import { OFF_GREY } from '../fallapp/constants/Colors';

Amplify.configure(awsmobile);

const App = props => {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => setLoadingComplete(true)}
      />
    );
  } else {
    return (
      <Root>
          <AppNavigator />
      </Root>
    
      // <Provider store={store}>
      //   {/* <PersistGate loading={null} persistor={persistor}> */}
      //     <StatusBar hidden />
      //     {/* <ActionSheetProvider> */}
      //       <Root>
      //         <View style={styles.container}>
      //           {Platform.OS === 'ios' && <StatusBar barStyle='default' />}

      //           <AppNavigator />
      //         </View>
      //       </Root>
      //     {/* </ActionSheetProvider> */}
      //   {/* </PersistGate> */}
      // </Provider>
    );
  }
};
async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Icon.Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Roboto_italics: require('./assets/fonts/Roboto-ThinItalic.ttf'),
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf')
    })
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: OFF_GREY
  }
});

export default withAuthenticator(App);
