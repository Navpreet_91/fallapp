import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { Button } from "react-native-elements";
import { HomeLogo, styles } from "./styles";
//Components
import DrawerWrapper from "../../components/DrawerWrapper";
import HeaderText from "../../components/HeaderText";

// Assets
const drawerImage = require("../../assets/transparent_logo.png");
const BaselineScreen = (props) => {
  //temporary code to show time and currenbt date with no logic attached
  const [minutes, setMinutes] = useState(10);
  const [seconds, setSeconds] = useState(0);

  useEffect(() => {
    let myInterval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(myInterval);
        } else {
          setMinutes(minutes - 1);
          setSeconds(59);
        }
      }
    }, 1000);
    return () => {
      clearInterval(myInterval);
    };
  });
  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    setCurrentDate(
      date + "/" + month + "/" + year + " " + hours + ":" + min + ":" + sec
    );
  }, []);

  return (
    <DrawerWrapper title="Carefall" backRoute="Home" padder>
      <View style={{ alignContent: "center", alignItems: "center" }}>
        <HeaderText title="Baseline Fall Risk" />
      </View>
      <Charts />
      <View style={{ alignContent: "center", alignItems: "center" }}>
        <HeaderText title="Monitoring Gait" />
      </View>
      <View>
        {minutes === 0 && seconds === 0 ? null : (
          <Text style={{ alignSelf: "center", fontSize: 16 }}>
            {" "}
            {minutes}:{seconds < 10 ? `0${seconds}` : seconds} remaining
          </Text>
        )}
      </View>

      <View
        style={{
          alignContent: "center",
          alignItems: "center",
          padding: 5,
          marginTop: 10,
        }}
      >
        <Text style={{ fontSize: 16, color: "#afafaf" }}>
          Last Update: {currentDate}
        </Text>
        {/* <HeaderText subtitle="Last Update: 25-02-2021 11:35:35" /> */}
      </View>
      <View>
        <Text
          style={{
            alignItems: "flex-start",
            padding: 5,
            marginTop: 10,
            lineHeight: 20,
          }}
        >
          Note: The app will monitor your gait in the background but please do
          not close the app to ensure th emost accurate reading is taken.
        </Text>
      </View>
    </DrawerWrapper>
  );
};

export default BaselineScreen;
