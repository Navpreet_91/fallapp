import styled from "styled-components";
import { DEVICE_HEIGHT, IMAGE_WIDTH } from "../../constants/Layout";
import { StyleSheet, Image } from "react-native";

import { MAIN_THEME_COLOR_1 } from "../../constants/Colors";

export const HomeLogo = styled(Image)`
  flex: 1;
  margin: auto;
  width: ${IMAGE_WIDTH};
  margin-bottom: 10%;
  align-self: center;
  resize-mode: contain;
  height: ${DEVICE_HEIGHT / 11};
`;

export const styles = StyleSheet.create({
  submitButton: {
    backgroundColor: "#6c757d",
    width: "30%",
    alignSelf: "center",
    margin: 10,
    borderStyle: "solid",
    borderColor: MAIN_THEME_COLOR_1,
    borderWidth: 1,
  },
  secondButton: {
    backgroundColor: "#6c757d",
    width: "70%",
    alignSelf: "center",
    margin: 10,
    borderStyle: "solid",
    borderColor: MAIN_THEME_COLOR_1,
    borderWidth: 1,
  },
  textStyle: { alignSelf: "center", fontSize: 16 },
  pickerStyle: {
    borderRadius: 10,
    borderWidth: 0.5,
    color: "#212529",
    height: 50,
    marginBottom: 10,
    backgroundColor: "#e5e6e4",
  },
});
