import React, { useState, useEffect, Component } from "react";
import { View, Text, TouchableOpacity, ActivityIndicator } from "react-native";
import { HomeLogo, styles } from "./styles";
import { Accelerometer } from "expo-sensors";
import Amplify, {Analytics, API, AWSKinesisFirehoseProvider, AWSKinesisProvider, graphqlOperation, DataStore, Auth} from 'aws-amplify';
import awsmobile from '../../src/aws-exports';
import { Button } from "react-native-elements"
//Components
import DrawerWrapper from "../../components/DrawerWrapper";
import HeaderText from "../../components/HeaderText";  
import { nativeViewProps } from "react-native-gesture-handler";
import * as subscriptions from "../../src/graphql/subscriptions";
import * as mutations from "../../src/graphql/mutations";
import RNSpeedometer from 'react-native-speedometer';

// Assets
const drawerImage = require("../../assets/transparent_logo.png");

Amplify.configure({
  Auth: {
    identityPoolId: awsmobile.aws_cognito_identity_pool_id,
    region: awsmobile.aws_project_region
  },
  Analytics: {
    AWSKinesisFirehose: {
      region: awsmobile.aws_project_region
    }
  },
  aws_appync_graphqlEndpoint: awsmobile.aws_appsync_graphqlEndpoint,
  aws_appsync_region: awsmobile.aws_appsync_region,
  aws_appsync_authenticationType: awsmobile.aws_appsync_authenticationType
});

//Amplify.configure(awsmobile)
Analytics.addPluggable(new AWSKinesisFirehoseProvider());




export default function MeterScreen(props) {
  //temporary code to show time and currenbt date with no logic attached
  const [minutes, setMinutes] = useState(10);
  const [seconds, setSeconds] = useState(0);
  const [fallRisk, setFallRisk] = useState(null);
  const [isLoading, setIsLoading] = useState(true)
  const [userEmail, setEmail] = useState(getEmail());
  const [wearableSelected, setWearableSelected] = useState(false)
  const [baselineRisk, setBaselineRisk] = useState(0);

  useEffect(() => {
    (async () => {
      const { attributes } = await Auth.currentUserInfo();
      const user_email = attributes.email
      console.log(user_email)
      setEmail(user_email)
    })()
    const device = props.navigation.getParam("device")
    if (device !== 'This phone'){
      setWearableSelected(true)
    }

    setBaselineRisk(props.navigation.getParam("baselineRisk"))

  }, []);

  function getEmail(){
    var email = async () => {
      const { attributes } = await Auth.currentUserInfo();
      console.log(attributes)
      const user_email = attributes.email
      setEmail(user_email)
      console.log(user_email)
      return user_email
    }
    return email
  }

  useEffect(() => {
    var result = getEmail()
    setEmail(result)
  }, []);

  function guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
  return (S4()+S4()+S4()+S4()+S4()+S4());
  }

  const unique_id = guidGenerator();
  //const userEmail = fetchEmail();

  useEffect(() => {
    try {
      var refreshId = setInterval(async () => {
        if (fallRisk === null && wearableSelected === false) {
          console.log("data to send", userEmail)
          const response = await fetch("https://7z504cexsj.execute-api.us-east-1.amazonaws.com/appsync-lambda-example?id=" + userEmail)
          const result = await response.json()
          console.log(result)
          const risk = result.getFallRisk
          if (result.getFallRisk === null ){
            console.log("nothing to show")
            return 
          } else if (result.getFallRisk !== null) {
            console.log(typeof new Date(start_date.start_date))
            console.log(typeof new Date(risk.date))
            // check to make sure we are getting a new entry
            if (new Date(risk.date) >= new Date(start_date.start_date)){
              console.log("we made it")
              console.log(risk)
              if (risk.risk === 1){
                setFallRisk("High")
              } else {
                setFallRisk("Low")
              }
              setIsLoading(false)
              clearInterval(refreshId)
            }
          }
        }
        
      }, 10000)
    } catch (err) {
      console.log(err)
    }
    }, []);

  const getUser = async() => {
    const user = await Auth.currentUserInfo();
    return user
  }

  useEffect(() => {
    let myInterval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1); 
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(myInterval);
        } else {
          setMinutes(minutes - 1);
          setSeconds(59);
        }
      }
    }, 1000);
    return () => {
      clearInterval(myInterval);
    };
  });
  const [currentDate, setCurrentDate] = useState("");
  

  useEffect(() => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    setCurrentDate(
      date + "/" + month + "/" + year + " " + hours + ":" + min + ":" + sec
    );
  }, []);

  const [data, setData] = useState({
    x: 0,
    y: 0,
    z: 0,
  });

  

  const [start_date, setStartDate] = useState({
    start_date: new Date(),
  });

  const [subscription, setSubscription] = useState(null);

  const _slow = () => {
    Accelerometer.setUpdateInterval(1000);
  };

  const _fast = () => {
    Accelerometer.setUpdateInterval(1);
  };



  const _subscribe = () => {
    try {
      console.log(userEmail)
      setSubscription(
        Accelerometer.addListener(async (accelerometerData) => {
          //console.log("test line #77");
          let dataToSend = {
            user_id: userEmail,
            date: new Date(),
            vals: accelerometerData
          };
          Analytics.record({
            data: dataToSend,
            streamName: 'accelerometer_data'
          }, 'AWSKinesisFirehose');
          setData(accelerometerData);
          
        })
      );
    } catch(e) {
      console.log(e)
    }
    
    
  };

  const _unsubscribe = () => {
    subscription && subscription.remove();
    setSubscription(null);
  };

  useEffect(() => {
    _subscribe();
    return () => _unsubscribe();
  }, []);

  const { x, y, z } = data;
  let loading;
  if (isLoading) {
    loading = <ActivityIndicator/>
  } else {
    loading = <Text style={{justifyContent:"center", 
                          alignSelf:"center", 
                          paddingVertical: 10,
                          fontSize: 26,
                          fontWeight:"bold"}}> 
                Fall Risk is: {fallRisk} 
              </Text>
  }

  const wearableView = 
    <DrawerWrapper title="Carefall" backRoute="Home" padder>
      <View style={{ alignContent: "center", alignItems: "center" }}>
        <HeaderText title="Baseline Fall Risk" />
      </View>
      <RNSpeedometer value={baselineRisk} size={200}/>
      <View style={{ alignContent: "center", alignItems: "center" }}>
        <HeaderText title="Monitoring Gait" />
      </View>
      
      <View>
        <Text style={{
            alignItems: "flex-start",
            padding: 5,
            marginTop: 10,
            lineHeight: 20,
            textAlign: 'center'
          }}>
          Please proceed on the wearable device you selected to monitor your gait. The result of your gait analysis will be displayed here.
        </Text>
      </View>
      
      <View>
        <Text
          style={{
            alignItems: "flex-start",
            padding: 5,
            marginTop: 10,
            lineHeight: 20,
            textAlign: "center"
          }}
        >
          Note: The app will monitor your gait in the background but please do
          not close the app to ensure th emost accurate reading is taken.
        </Text>
      </View>

      <View style={{justifyContent:"center", paddingVertical:10}}>
        {loading}
      </View>

      <View>
        <Button
            title="Home"
            buttonStyle={styles.mainButton}
            onPress={() => props.navigation.push("Info")}
          />
      </View>
      
    </DrawerWrapper>

  if (wearableSelected){
    return wearableView
  } else {

  

  
  return (
    <DrawerWrapper title="Carefall" backRoute="Home" padder>
      {/* <HomeLogo square source={drawerImage} /> */}
      <View style={{ alignContent: "center", alignItems: "center" }}>
        <HeaderText title="Baseline Fall Risk" />
      </View>
      <View>
        <RNSpeedometer value={baselineRisk} size={200} labels= {[
          {
            name: 'Low',
            labelColor: '#14eb6e',
            activeBarColor: '#14eb6e',
          },

          {
            name: 'Normal',
            labelColor: '#f2cf1f',
            activeBarColor: '#f2cf1f',
          },
          {
            name: 'High',
            labelColor: '#ff2900',
            activeBarColor: '#ff2900',
          }]}/>
      </View>
      <View style={{ alignContent: "center", alignItems: "center", paddingTop: 50 }}>
        <HeaderText title="Monitoring Gait" />
      </View>
      <View>
        {minutes === 0 && seconds === 0 ? null : (
          <Text style={{ alignSelf: "center", fontSize: 16 }}>
            {" "}
            {minutes}:{seconds < 10 ? `0${seconds}` : seconds} remaining
          </Text>
        )}
      </View>
      <View
        style={{
          alignContent: "center",
          alignItems: "center",
          padding: 5,
          marginTop: 10,
        }}
      >
        <Text style={{ fontSize: 16, color: "#afafaf" }}>
          Last Update: {currentDate}
        </Text>
        {/* <HeaderText subtitle="Last Update: 25-02-2021 11:35:35" /> */}
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          borderWidth: 1,
          marginBottom: 10,
        }}
      >
        <HeaderText title="x" subtitle={round(x)} />
        <HeaderText title="y" subtitle={round(y)} />
        <HeaderText title="z" subtitle={round(z)} />
      </View>
      <View>
        <TouchableOpacity
          onPress={subscription ? _unsubscribe : _subscribe}
          style={styles.mainButton}
        >
          <Text style={{ color: "white", fontSize: 18 }}>
            {subscription ? "On" : "Off"}
          </Text>
        </TouchableOpacity>
        <View style={{justifyContent:"center", paddingVertical:10}}>
          {loading}
        </View>

        
        
        {/* <View style={styles.buttonContainer}>
          <TouchableOpacity onPress={_slow} style={styles.secondButton}>
            <Text>Slow</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={_fast} style={styles.secondButton}>
            <Text>Fast</Text>
          </TouchableOpacity>
        </View> */}
      </View>
      <View>
        <Text
          style={{
            alignItems: "flex-start",
            padding: 5,
            marginTop: 10,
            lineHeight: 20,
          }}
        >
          Note: The app will monitor your gait in the background but please do
          not close the app to ensure th emost accurate reading is taken.
        </Text>
      </View>
      <View>
        <Button
            title="Home"
            buttonStyle={styles.mainButton}
            onPress={() => props.navigation.push("Info")}
          />
      </View>
    </DrawerWrapper>
  );
        }
}

function round(n) {
  if (!n) {
    return 0;
  }
  return Math.floor(n * 100) / 100;
}

// export default MeterScreen;
