import styled from "styled-components";
import { DEVICE_HEIGHT, IMAGE_WIDTH } from "../../constants/Layout";
import { StyleSheet, Image } from "react-native";

import { MAIN_THEME_COLOR_1 } from "../../constants/Colors";

export const HomeLogo = styled(Image)`
  flex: 1;
  margin: auto;
  width: ${IMAGE_WIDTH};
  margin-bottom: 10%;
  align-self: center;
  resize-mode: contain;
  height: ${DEVICE_HEIGHT / 11};
`;

export const styles = StyleSheet.create({
  mainButton: {
    backgroundColor: MAIN_THEME_COLOR_1,
    width: "30%",
    alignSelf: "center",
    borderStyle: "solid",
    borderColor: MAIN_THEME_COLOR_1,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
  },
  secondButton: {
    borderStyle: "solid",
    borderColor: MAIN_THEME_COLOR_1,
    borderWidth: 1,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#eee",
    padding: 10,
  },
  buttonContainer: {
    flexDirection: "row",
    alignItems: "stretch",
    marginTop: 15,
  },
  textStyle: {
    alignSelf: "center",
    margin: 5,
    fontSize: 20,
    fontWeight: "700",
    textTransform: "uppercase",
  },
  text: {
    alignSelf: "center",
    fontSize: 20,
    alignItems: "flex-start",
  },
});
