import React from "react";
import { View, Text, ScrollView, Linking } from "react-native";
import { Button } from "react-native-elements";
import DrawerWrapper from "../../components/DrawerWrapper";
import HeaderText from "../../components/HeaderText";
//styles
import { styles } from "./styles";

const screen2 = (props) => {
  return (
    <DrawerWrapper title="Exercises" padder>
      <ScrollView>
        <HeaderText
          title="Sit to Stand"
          textStyle={{
            justifyContent: "center",
            color: "black",
            alignSelf: "center",
          }}
        />
        <Text style={styles.textStyle}>
          The sit-to-stand exercises builds leg strength and improves body
          mechanics and balance, which are all important in reducing fails.
        </Text>
        <Text style={styles.textStyle}>
          𝟏.Start by sitting on asturdy chair of standard height, and make sure
          that it won't side or roll.You should be able to sit comfortably with
          your feet falt on the ground. Have a sturdy support surface in front
          of you, such as a countertop, so that you can reach to it for support
          if you start to feel unsteady when standing. Scoot forward so your
          buttock is positioned at the frnt of the seat.
        </Text>
        <Text style={styles.textStyle}>
          𝟐.Lean your chest forward over your toes,shifting your body weight
          forward.Sequeeze your gluteal muscles and slowly rise to a stable
          standing position.
        </Text>
        <Text style={styles.textStyle}>
          𝟑.Slowly sit back down to the starting position and repeat 10 times.
        </Text>
        <Text style={styles.textStyle}>
          𝟒.If necessary, place your hands on the arms or seat of the chair and
          push through your hands to help stand and sit.The goal is to not use
          your hands at all.
        </Text>
        <Text style={styles.textStyle}>
          Perform 10 repetitions, twice a day.For an advanced version,hold hand
          weights to add resistance.
        </Text>

        <View style={{ marginTop: 10 }}>
          <Button
            title="Next"
            buttonStyle={styles.secondButton}
            onPress={() => props.navigation.push("Screen3")}
          />
        </View>
      </ScrollView>
    </DrawerWrapper>
  );
};
export default screen2;
