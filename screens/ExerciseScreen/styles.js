import styled from "styled-components";
import { DEVICE_HEIGHT, IMAGE_WIDTH } from "../../constants/Layout";
import { StyleSheet, Image } from "react-native";

import { MAIN_THEME_COLOR_1 } from "../../constants/Colors";

export const styles = StyleSheet.create({
  secondButton: {
    backgroundColor: MAIN_THEME_COLOR_1,
    width: "70%",
    alignSelf: "center",
    margin: 10,
    borderStyle: "solid",
    borderColor: MAIN_THEME_COLOR_1,
    borderWidth: 1,
    borderRadius: 10,
  },
  textStyle: {
    alignSelf: "center",
    fontSize: 17,
    padding: 5,
    lineHeight: 21,
    textAlign: "justify",
  },
  linkStyle:{
    alignSelf: "center",
    fontSize: 18,
    padding: 10,
    lineHeight: 22,
    textAlign: "justify",
    color:'blue' ,
    textDecorationLine:'underline' 
  }
});
