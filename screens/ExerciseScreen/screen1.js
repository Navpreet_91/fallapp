import React from "react";
import { View, Text, ScrollView, Linking } from "react-native";
import { Button } from "react-native-elements";
import DrawerWrapper from "../../components/DrawerWrapper";
import HeaderText from "../../components/HeaderText";
//styles
import { styles } from "./styles";

const openLink=()=>{
    Linking.openURL("https://www.hopkinsmedicine.org/health/wellness-and-prevention/fall-prevention-exercises")
}

const screen1 = (props) => {
  return (
    <DrawerWrapper title="Carefall" padder>
      <ScrollView>
        <HeaderText
          title="Exercises"
          textStyle={{
            justifyContent: "center",
            color: "black",
            alignSelf: "center",
            marginTop: 50,
          }}
        />
        <Text style={styles.textStyle}>
          We have included some exercises in our app in order to help you not
          only understand your risk, but also to reduce it. Click below to see
          some of the exercises and get started!
        </Text>
        <Text style={styles.textStyle}>
          Note:The exercise recommended here are directly from Johhns Hopkins
          Medicine. See the link below for more details.
        </Text>
        <Text
          style={styles.linkStyle}
          onPress={openLink}
        >
          https://www.hopkinsmedicine.org/health/wellness-and-prevention/fall-prevention-exercises
        </Text>
        <View style={{ marginTop: 20 }}>
          <Button
            title="Start Exercises"
            buttonStyle={styles.secondButton}
            onPress={() => props.navigation.push("Screen2")}
          />
        </View>
      </ScrollView>
    </DrawerWrapper>
  );
};
export default screen1;
