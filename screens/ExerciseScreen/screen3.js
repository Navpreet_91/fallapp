import React from "react";
import { View, Text, ScrollView, Linking } from "react-native";
import { Button } from "react-native-elements";
import DrawerWrapper from "../../components/DrawerWrapper";
import HeaderText from "../../components/HeaderText";
//styles
import { styles } from "./styles";

const screen3 = (props) => {
  return (
    <DrawerWrapper title="Exercises" padder>
      <ScrollView>
        <HeaderText
          title="Balance"
          textStyle={{
            justifyContent: "center",
            color: "black",
            alignSelf: "center",
          }}
        />
        <Text style={styles.textStyle}>
          To begin, stand in a corner or have a kitchen counter in front of you
          to reach out to in case you start losing balance.
        </Text>
        <Text style={styles.textStyle}>
          𝟏.Feet apart:Stand with feet about shoulder-width apart, eyes open,
          and hold steady for 10 seconds, working your way up to 30 seconds.
        </Text>
        <Text style={styles.textStyle}>
          If you find yourself swaying or reaching for the wall or counter
          fequently,just keep working on this exercise until you can do it with
          minimal swaying or support.Once you can hold this position firmly for
          30 seconds, move on to the next exercise.
        </Text>
        <Text style={styles.textStyle}>
          𝟐.Feet together:Stand with feet together,eyes open, and hold steady 10
          seconds, working your way up to 30 seconds.
        </Text>
        <Text style={styles.textStyle}>
          𝟑.One foot: Stand on one foot, eyes open,and hold steady 10
          seconds,working up to 30 seconds.Switch to the other foot.
        </Text>
        <Text style={styles.textStyle}>
          𝟒.Eyes closed:If you can perform the first three exercises safely and
          with little support, try to do each one with your eyes closed. Hold
          for 10 seconds, working up to 30 seconds.
        </Text>

        <View style={{ marginTop: 10 }}>
          <Button
            title="Home"
            buttonStyle={styles.secondButton}
            onPress={() => props.navigation.push("Home")}
          />
        </View>
      </ScrollView>
    </DrawerWrapper>
  );
};
export default screen3;
