
export const duration = [
    { label: "10 minutes", key: "0" },
    { label: "30 minutes", key: "1" },
    { label: "1 hour", key: "2" },
  ];