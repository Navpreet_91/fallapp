import React, { useState } from "react";
import { ScrollView, View } from "react-native";
import { Button, Tooltip, Text } from "react-native-elements";
import { Card, CardItem } from "native-base";
import { MaterialIcons } from "@expo/vector-icons";
//components
import DrawerWrapper from "../../components/DrawerWrapper";
import HeaderText from "../../components/HeaderText";
import DropDown from "../../components/DropDown";
//styles

import { HomeLogo, styles } from "./styles";

// Assets
const drawerImage = require("../../assets/transparent_logo.png");

const HomeScreen = (props) => {
  const [selectedAge, setSelectedAge] = useState();
  const [selectedTime, setSelectedTime] = useState();
  const [selectedTension, setSelectedTension] = useState();
  const [selectedFalls, setSelectedFalls] = useState();
  const [selectedMed, setSelectedMed] = useState();
  const [selectedDuration, setSelecetedDuration] = useState();
  const [selectDevice, setSelectedDevice] = useState();
  const [baselineRisk, setBaselineRisk] = useState();

  const falls = [
    { label: "0", key: "0" },
    { label: "1", key: "1" },
    { label: "2", key: "2" },
    { label: "3", key: "3" },
    { label: "4", key: "4" },
    { label: "5+", key: "5" },
  ];
  const med = [
    { label: "0", key: "0" },
    { label: "1", key: "1" },
    { label: "2", key: "2" },
    { label: "3", key: "3" },
    { label: "4", key: "4" },
    { label: "5+", key: "5" },
  ];

  const tension = [
    { label: "No", key: "0" },
    { label: "Yes", key: "1" },
  ];
  const timed = [
    { label: "4 Seconds", key: "0" },
    { label: "5 Seconds", key: "1" },
    { label: "6 Seconds", key: "2" },
    { label: "7 Seconds", key: "3" },
    { label: "8 Seconds", key: "4" },
    { label: "9 Seconds", key: "5" },
    { label: "10 Seconds", key: "6" },
    { label: "11 Seconds", key: "7" },
    { label: "12 Seconds", key: "8" },
    { label: "13 Seconds", key: "9" },
    { label: "14 Seconds", key: "10" },
    { label: "15 Seconds", key: "11" },
    { label: "16 Seconds", key: "12" },
    { label: "17 Seconds", key: "13" },
    { label: "18 Seconds", key: "14" },
    { label: "19 Seconds", key: "15" },
    { label: "20 Seconds", key: "16" },
  ];

  const options = [
    { label: "30 years", key: "0" },
    { label: "31 years", key: "1" },
    { label: "32 years", key: "2" },
    { label: "33 years", key: "3" },
    { label: "34 years", key: "4" },
    { label: "35 years", key: "5" },
    { label: "36 years", key: "6" },
    { label: "37 years", key: "7" },
    { label: "38 years", key: "8" },
    { label: "39 years", key: "9" },
    { label: "40 years", key: "10" },
    { label: "41 years", key: "11" },
    { label: "42 years", key: "12" },
    { label: "43 years", key: "13" },
    { label: "44 years", key: "14" },
    { label: "45 years", key: "15" },
    { label: "46 years", key: "16" },
    { label: "47 years", key: "17" },
    { label: "48 years", key: "18" },
    { label: "49 years", key: "19" },
    { label: "50 years", key: "20" },
    { label: "51 years", key: "21" },
    { label: "52 years", key: "22" },
    { label: "53 years", key: "23" },
    { label: "54 years", key: "24" },
    { label: "55 years", key: "25" },
    { label: "56 years", key: "26" },
    { label: "57 years", key: "27" },
    { label: "58 years", key: "28" },
    { label: "59 years", key: "29" },
    { label: "60 years", key: "30" },
    { label: "61 years", key: "31" },
    { label: "62 years", key: "32" },
    { label: "63 years", key: "33" },
    { label: "64 years", key: "34" },
    { label: "65 years", key: "35" },
    { label: "66 years", key: "36" },
    { label: "67 years", key: "37" },
    { label: "68 years", key: "38" },
    { label: "69 years", key: "39" },
    { label: "70 years", key: "40" },
    { label: "71 years", key: "41" },
    { label: "72 years", key: "42" },
    { label: "73 years", key: "43" },
    { label: "74 years", key: "44" },
    { label: "75 years", key: "45" },
    { label: "76 years", key: "46" },
    { label: "77 years", key: "47" },
    { label: "78 years", key: "48" },
    { label: "79 years", key: "49" },
    { label: "80 years", key: "50" },
    { label: "81 years", key: "51" },
    { label: "82 years", key: "52" },
    { label: "83 years", key: "53" },
    { label: "84 years", key: "54" },
    { label: "85 years", key: "55" },
    { label: "86 years", key: "56" },
    { label: "87 years", key: "57" },
    { label: "88 years", key: "58" },
    { label: "89 years", key: "59" },
    { label: "90 years", key: "60" },
    { label: "91 years", key: "61" },
    { label: "92 years", key: "62" },
    { label: "93 years", key: "63" },
    { label: "94 years", key: "64" },
    { label: "95 years", key: "65" },
    { label: "96 years", key: "66" },
    { label: "97 years", key: "67" },
    { label: "98 years", key: "68" },
    { label: "99 years", key: "69" },
    { label: "100 years", key: "70" },
  ];

  const duration = [
    { label: "10 minutes", key: "0" },
  ];

  function calculateBaselineRisk(){
    var score = 0
    var age = selectedAge.split(" ")[0]

    var posturalHypotension = 0;

    if (selectedTension === "Yes"){
      posturalHypotension = 1
    } else {
      posturalHypotension = 0
    }

    var timedTest = selectedTime.split(" ")[0]

    score += Number(selectedFalls)

    if (timedTest >= 14) {
      score += Number(1)
    }

    if (age > 80) {
      score += Number(3)
    } else if (age > 75 && age <= 80){
      score += Number(2)
    } else if (age > 70 && age <= 75){
      score += Number(1)
    }

    if (posturalHypotension === 1) {
      score += Number(1)
    }

    if (selectedMed >= 2){
      score += Number(1)
    }

    score = (score/11)*100
    console.log("baseline risk", score)

    props.navigation.push("Meter", {'device': selectDevice, 'baselineRisk': score})
  }

  const device = [
    { label: "Smartwatch", key: "0" },
    { label: "This phone", key: "1" },
  ];
  return (
    <DrawerWrapper title="Carefall" padder>
      <ScrollView>
        {/* <HomeLogo square source={drawerImage} /> */}
        <HeaderText
          title="Step 1: Calculate Baseline Risk"
          subtitle="Press on bar to select value"
        />
        <View style={{ flexDirection: "row" }}>
          <DropDown
            value={selectedAge}
            placeholder="Select Age"
            data={options}
            onChange={(option) => setSelectedAge(option.label)}
          />
          <Tooltip
            containerStyle={{ backgroundColor: "#25BEED" }}
            height={50}
            width={250}
            pointerColor="#25BEED"
            popover={
              <Text style={{ fontWeight: "bold" }}>Select your Age.</Text>
            }
          >
            <MaterialIcons
              name="help"
              style={{ color: "#25BEED", margin: 5 }}
              size={24}
            />
          </Tooltip>
        </View>
        <View style={{ flexDirection: "row" }}>
          <DropDown
            value={selectedFalls}
            placeholder="Number of previous falls"
            data={falls}
            onChange={(option) => setSelectedFalls(option.label)}
          />
          <Tooltip
            containerStyle={{ backgroundColor: "#25BEED" }}
            height={100}
            width={250}
            pointerColor="#25BEED"
            popover={
              <View>
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 16,
                    alignSelf: "center",
                  }}
                >
                  Number of Previous Falls
                </Text>
                <Text>
                  Enter the number of falls that you have ever experienced.
                </Text>
              </View>
            }
          >
            <MaterialIcons
              name="help"
              style={{ color: "#25BEED", margin: 5 }}
              size={24}
            />
          </Tooltip>
        </View>
        <View style={{ flexDirection: "row" }}>
          <DropDown
            value={selectedMed}
            placeholder="Number of Psychoactive Medications"
            data={med}
            onChange={(option) => setSelectedMed(option.label)}
          />
          <Tooltip
            containerStyle={{ backgroundColor: "#25BEED" }}
            height={280}
            width={250}
            pointerColor="#25BEED"
            popover={
              <View>
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 16,
                    alignSelf: "center",
                  }}
                >
                  Psychoactive Medications
                </Text>
                <Text>
                  Enter the number of psychoactive medications that you are
                  currently taking.
                </Text>
                <Text style={{ fontWeight: "bold", margin: 5, fontSize: 15 }}>
                  Psychoactive Medications
                </Text>
                <Text>- Antidepressants</Text>
                <Text>- Benzodiazepines</Text>
                <Text>- Barbituates</Text>
                <Text>- Hydroxyzine</Text>
                <Text>- Phenothiazines</Text>
                <Text>-Butyrophenones</Text>
                <Text>-Cholral hydrate</Text>
              </View>
            }
          >
            <MaterialIcons
              name="help"
              style={{ color: "#25BEED", margin: 5 }}
              size={24}
            />
          </Tooltip>
        </View>
        <View style={{ flexDirection: "row" }}>
          <DropDown
            placeholder="Postural Hypotension"
            data={tension}
            value={selectedTension}
            onChange={(option) => setSelectedTension(option.label)}
          />
          <Tooltip
            containerStyle={{ backgroundColor: "#25BEED" }}
            height={260}
            width={250}
            pointerColor="#25BEED"
            popover={
              <View>
                <Text
                  style={{
                    fontWeight: "bold",
                    margin: 5,
                    fontSize: 16,
                    alignSelf: "center",
                  }}
                >
                  Postural Hypotension
                </Text>
                <Text style={{ fontWeight: "bold", margin: 5, fontSize: 15 }}>
                  Instructions
                </Text>
                <Text style={{ fontSize: 15, lineHeight: 18 }}>
                  Take blood pressure from seated position. Stand up and take
                  blood pressure while standing within 3 minutes of standing up.
                  {"\n"}
                  If there is a decrease in systolic blood pressure of 20 mm Hg
                  or a decrese in diastolic blood pressure of 10mm Hg within
                  three minutes of standing comapred with blood pressure from
                  the sitting position,select yes.
                </Text>
              </View>
              // <Card>
              //   <CardItem header bordered>
              //     <Text style={{ fontWeight: "bold" }}>
              //       Postural Hypotension
              //     </Text>
              //   </CardItem>
              //   <CardItem>
              //     <Text style={{ fontWeight: "bold" }}>Instructions</Text>
              //   </CardItem>
              //   <CardItem footer bordered>
              //     <Text>
              //       Take blood pressure from seated position. Stand up and take
              //       blood pressure while standing within 3 minutes of standing
              //       up.
              //       {"\n"}
              //       If there is a decrease in systolic blood pressure of 20 mm
              //       Hg or a decrese in diastolic blood pressure of 10mm Hg
              //       within three minutes of standing comapred with blood
              //       pressure from the sitting position,select yes.
              //     </Text>
              //   </CardItem>
              // </Card>
            }
          >
            <MaterialIcons
              name="help"
              style={{ color: "#25BEED", margin: 5 }}
              size={24}
            />
          </Tooltip>
        </View>
        <View style={{ flexDirection: "row" }}>
          <DropDown
            placeholder="Timed Up and Go Test"
            data={timed}
            value={selectedTime}
            onChange={(option) => setSelectedTime(option.label)}
          />

          <Tooltip
            containerStyle={{ backgroundColor: "#25BEED" }}
            height={300}
            width={250}
            pointerColor="#25BEED"
            popover={
              <View>
                <Text
                  style={{
                    fontWeight: "bold",
                    margin: 5,
                    fontSize: 16,
                    alignSelf: "center",
                  }}
                >
                  Timed Up & Go Test
                </Text>
                <Text>
                  A simple test that is used to asses a person's mobility.
                </Text>
                <Text style={{ fontWeight: "bold", margin: 5, fontSize: 15 }}>
                  Instructions
                </Text>
                <Text>Sit in a chair</Text>
                <Text>Stand up and walk 3 metres</Text>
                <Text>Turn around</Text>
                <Text>Walk back to chair</Text>
                <Text>Sit down</Text>
                <Text style={{ fontWeight: "bold", margin: 5, fontSize: 15 }}>
                  Score
                </Text>
                <Text>Total time for standing up to sitting down.</Text>
              </View>
            }
          >
            <MaterialIcons
              name="help"
              style={{ color: "#25BEED", margin: 5 }}
              size={24}
            />
          </Tooltip>
        </View>
        <HeaderText
          title="Step 2: Setup RealTime Risk"
          subtitle="Select duration of real time monitoring"
        />
        <DropDown
          placeholder="Select monitoring duration "
          data={duration}
          value={'10 minutes'}
          onChange={(option) => setSelecetedDuration(option.label)}
        />
        <HeaderText
          title="Step 3: Select Device"
          subtitle="Select device to use for monitoring"
        />
        <DropDown
          placeholder="Select device "
          data={device}
          value={selectDevice}
          onChange={(option) => setSelectedDevice(option.label)}
        />
        <Button
          title="BEGIN FALL MONITORING"
          buttonStyle={styles.secondButton}
          onPress={() => calculateBaselineRisk()}
        />
      </ScrollView>
    </DrawerWrapper>
  );
};

export default HomeScreen;
