import styled from "styled-components";
import { DEVICE_HEIGHT, IMAGE_WIDTH } from "../../constants/Layout";
import { StyleSheet, Image } from "react-native";

import { MAIN_THEME_COLOR_1 } from "../../constants/Colors";

export const HomeLogo = styled(Image)`
  flex: 1;
  margin: auto;
  width: ${IMAGE_WIDTH};
  margin-bottom: 8%;
  align-self: center;
  resize-mode: contain;
  height: ${DEVICE_HEIGHT / 11};
`;

export const styles = StyleSheet.create({
  submitButton: {
    backgroundColor: "#6c757d",
    width: "30%",
    alignSelf: "center",
    margin: 10,
    borderStyle: "solid",
    borderColor: MAIN_THEME_COLOR_1,
    borderWidth: 1,
  },
  secondButton: {
    backgroundColor: MAIN_THEME_COLOR_1,
    width: "70%",
    alignSelf: "center",
    margin: 10,
    borderStyle: "solid",
    // borderColor: MAIN_THEME_COLOR_1,
    borderWidth: 1,
  },
  textStyle: { alignSelf: "center", margin: 5, fontSize: 16 },
  containerStyle: {
    flex: 1,
    justifyContent: "space-around",
    marginBottom: 10,
    backgroundColor: "#e5e6e4",
    // borderWidth: 1,
    borderColor: "#212529",
    borderRadius:10,
  },
  pickerStyle: {
    borderWidth: 1,
    borderColor: "#212529",
    borderRadius:10,
    padding: 5,
    fontSize: 16,
  },
});
