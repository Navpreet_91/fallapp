import { StyleSheet } from "react-native";

import { MAIN_THEME_COLOR_1 } from "../../constants/Colors";

export const styles = StyleSheet.create({
  secondButton: {
    backgroundColor: MAIN_THEME_COLOR_1,
    width: "70%",
    alignSelf: "center",
    margin: 10,
    borderStyle: "solid",
    borderColor: MAIN_THEME_COLOR_1,
    borderWidth: 1,
    borderRadius: 10,
  },
  textStyle: {
    alignSelf: "center",
    fontSize: 18,
    padding: 10,
    lineHeight: 22,
    textAlign: "justify",
  },
});
