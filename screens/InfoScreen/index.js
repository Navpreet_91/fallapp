import React from "react";
import { ScrollView, Text ,View} from "react-native";
import { Button } from "react-native-elements";
import { Auth } from "aws-amplify"
//components
import DrawerWrapper from "../../components/DrawerWrapper";
import HeaderText from "../../components/HeaderText";

//styles
import { styles } from "./styles";

// Assets
const drawerImage = require("../../assets/transparent_logo.png");



const InfoScreen = (props) => {
  const user_email = null;
  const user = Auth.currentAuthenticatedUser().then(user => user_email = user);


  
  return (
    <DrawerWrapper title="Carefall" padder>
      <ScrollView>
        <HeaderText
          title="Welcome to CareFall"
          textStyle={{
            justifyContent: "center",
            color: "black",
            alignSelf: "center",
            marginTop: 50,
          }}
        />
        <Text style={styles.textStyle}>
          We need some information from you to evaluate your fall risk. We will
          ask you to fill out some questions and then we will monitor your
          walking patterns. Click below to get started.
        </Text>
        <View style={{marginTop:100}}>
        <Button
          title="Start Assessment"
          buttonStyle={styles.secondButton}
          onPress={() => props.navigation.push("Home")}
        />
             <Button
          title="Reduce my Risk"
          buttonStyle={styles.secondButton}
          onPress={() => props.navigation.push("Screen1")}
        />
        </View>
  
      </ScrollView>
    </DrawerWrapper>
  );
};

export default InfoScreen;
