package com.example.aws.kinesis.android.mobile_streams.util

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

/**
 * @author Rishabh Tatiraju
 *
 * An interface to used as the Retrofit service
 * Holds all APIs
 *
 * How to use:
 * @sample ApiAdapter.apiClient.getRandomDogImage()
 * */
interface ApiClient {
    @GET("/verify_email")
    suspend fun getLoginResponse(@Query("email") key: String): Response<LoginModel>

}
