package com.example.aws.kinesis.android.mobile_streams

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import com.example.aws.kinesis.android.mobile_streams.util.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity(), CoroutineScope by MainScope() {
    var progressBar: ContentLoadingProgressBar? = null
    var loginEmail = "";
    var loginPwd = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Preferences.setContext(this);
        progressBar = findViewById(R.id.progressBar) as ContentLoadingProgressBar;
        var serviceStatus = Preferences.getString(Constants.SERVICE_STATUS, "STOPPED");
        if (serviceStatus == "STOPPED" ) {
            var etEmail = findViewById(R.id.editTextEmail) as EditText
            var startSensorStream = findViewById(R.id.btnStartSensor) as Button

            startSensorStream.setOnClickListener {
                var email = etEmail.text.trim().toString();
                if (email.length > 2) {
                    launch(Dispatchers.Main) {
                        // Try catch block to handle exceptions when calling the API.
                        try {
                            val response = ApiAdapter.apiClient.getLoginResponse(email)

                            if (response.isSuccessful && response.body() != null) {
                                val data = response.body()!!
                                if (data.user_exists == "true") {
                                    Preferences.update("user_email", email);
                                    val intent = Intent(this@MainActivity, StartStream::class.java)
                                    startActivity(intent)
                                }
                                else
                                {
                                    progressBar!!.setVisibility(View.GONE)
                                    AlertDialog.Builder(this@MainActivity).setTitle("Email not found!")
                                            .setMessage("Please sign up on the mobile app to continue")
                                            .setPositiveButton(android.R.string.ok) { _, _ -> }
                                            .setIcon(android.R.drawable.ic_dialog_alert).show()
                                }
                            } else {
                                    progressBar!!.setVisibility(View.GONE)
                                    AlertDialog.Builder(this@MainActivity).setTitle("Email not found!")
                                            .setMessage("Please sign up on the mobile app to continue")
                                            .setPositiveButton(android.R.string.ok) { _, _ -> }
                                            .setIcon(android.R.drawable.ic_dialog_alert).show()

                            }
                        } catch (e: Exception) {
                            // Show API error. This is the error raised by the client.
                            Toast.makeText(this@MainActivity,
                                    "Error Occurred: ${e.message}",
                                    Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }
        else
        {
            val intent = Intent(this, StartStream::class.java)
            startActivity(intent)
        }
    }


    private fun isNetworkConnected(): Boolean {
        //1
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        //2
        val activeNetwork = connectivityManager.activeNetwork
        //3
        val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)
        //4
        return networkCapabilities != null &&
                networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    }
}