package com.example.aws.kinesis.android.mobile_streams.util

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Rishabh Tatiraju
 *
 * This static class holds all settings for Retrofit.
 **/

object ApiAdapter {
    // Interface to be used as Retrofit service.
    // We're using Dog.ceo public API.
    val apiClient: ApiClient = Retrofit.Builder()
            .baseUrl("https://jhmzlo8pr3.execute-api.us-east-1.amazonaws.com")
            .client(OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiClient::class.java)
}
