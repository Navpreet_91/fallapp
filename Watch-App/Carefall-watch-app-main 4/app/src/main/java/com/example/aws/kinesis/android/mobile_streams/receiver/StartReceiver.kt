package com.example.aws.kinesis.android.mobile_streams.service;

import android.content.Context
import android.content.BroadcastReceiver
import android.content.Intent
import android.os.Build
import com.example.aws.kinesis.android.mobile_streams.EndlessService
import com.example.aws.kinesis.android.mobile_streams.util.Actions

class StartReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
            Intent(context, EndlessService::class.java).also {
                it.action = Actions.START.name
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(it)
                    return
                }
                context.startService(it)
            }
        }
    }
}
