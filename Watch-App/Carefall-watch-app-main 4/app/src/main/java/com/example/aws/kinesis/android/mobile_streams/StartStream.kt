package com.example.aws.kinesis.android.mobile_streams

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.aws.kinesis.android.mobile_streams.util.*

class StartStream : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_startstream)
        Preferences.setContext(this);
        var serviceStatus = Preferences.getString(Constants.SERVICE_STATUS, "STOPPED");
        var startSensorStream = findViewById(R.id.btnStartSensor) as Button
        if (serviceStatus == "STOPPED" ) {
            startSensorStream.setOnClickListener {
                actionOnService(Actions.START)
                startSensorStream.text = this.getResources().getString(R.string.stop_sensor_stream)
            }
        }
        else
        {
            startSensorStream.text = this.getResources().getString(R.string.stop_sensor_stream)
            startSensorStream.setOnClickListener {
                actionOnService(Actions.STOP)
                startSensorStream.text = this.getResources().getString(R.string.start_sensor_stream)
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun actionOnService(action: Actions) {
        if (getServiceState(this) == ServiceState.STOPPED && action == Actions.START) {
            Intent(this, EndlessService::class.java).also {
                it.action = action.name
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(it)
                    return
                }
                startService(it)
            }
        }
    }
}