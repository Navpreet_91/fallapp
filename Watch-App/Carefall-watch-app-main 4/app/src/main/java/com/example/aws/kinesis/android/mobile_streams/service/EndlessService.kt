package com.example.aws.kinesis.android.mobile_streams;

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.Sensor.TYPE_ACCELEROMETER
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.*
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import com.amazonaws.AmazonClientException
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.kinesis.kinesisrecorder.KinesisFirehoseRecorder
import com.amazonaws.mobileconnectors.kinesis.kinesisrecorder.KinesisRecorder
import com.amazonaws.regions.Regions
import com.example.aws.kinesis.android.mobile_streams.util.Actions
import com.example.aws.kinesis.android.mobile_streams.util.Constants
import com.example.aws.kinesis.android.mobile_streams.util.Preferences
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors


class EndlessService : Service(), SensorEventListener {
    private var wakeLock: PowerManager.WakeLock? = null
    private var isServiceStarted = false
    private lateinit var mSensorManager: SensorManager
    private var mAccelerometerSensor: Sensor? = null
    var userEmail: String? = ""
    var acceleratorDataString: String? = ""
    var sBuilder = StringBuilder()
    var androidId: String? = null
    var cognitoIdentityPoolId: String? = null
    var region: Regions? = null
    var kinesisStreamName: String? = null
    var firehoseStreamName: String? = null

    val APPLICATION_NAME = "android-mobile-streams"
    val RAD2DEG = 180 / Math.PI
    var kinesisRecorder: KinesisRecorder? = null
    var firehoseRecorder: KinesisFirehoseRecorder? = null
    private val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")


    override fun onBind(intent: Intent): IBinder? {
        // We don't provide binding, so return null
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            val action = intent.action
            when (action) {
                Actions.START.name -> startService()
                Actions.STOP.name -> stopService()
            }
        }
        Preferences.setContext(applicationContext);
        userEmail = Preferences.getString("user_email", "")
        // by returning this we make sure the service is restarted if the system kills the service
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        val notification = createNotification()
        startForeground(1, notification)
        initRecorder()
        requestHeartRateAndStepCount()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopMeasure()
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        val restartServiceIntent = Intent(applicationContext, EndlessService::class.java).also {
            it.setPackage(packageName)
        };
        val restartServicePendingIntent: PendingIntent =
            PendingIntent.getService(this, 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        applicationContext.getSystemService(Context.ALARM_SERVICE);
        val alarmService: AlarmManager =
            applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager;
        alarmService.set(
            AlarmManager.ELAPSED_REALTIME,
            SystemClock.elapsedRealtime() + 1000,
            restartServicePendingIntent
        );
    }

    private fun startService() {
        if (isServiceStarted) return
        Toast.makeText(this, "Service starting its task", Toast.LENGTH_SHORT).show()
        isServiceStarted = true
        Preferences.setContext(applicationContext);
        Preferences.update(Constants.SERVICE_STATUS, "STARTED");
        // we need this lock so our service gets not affected by Doze Mode
        wakeLock =
            (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
                newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "EndlessService::lock").apply {
                    acquire()
                }
            }
        // we're starting a loop in a coroutine
        GlobalScope.launch(Dispatchers.IO) {
            while (isServiceStarted) {
                launch(Dispatchers.IO) {
                    registerDevice()
                }
                delay(20 * 60 * 1000)
            }
        }
    }

    fun log(msg: String) {
        //Log.d("ENDLESS-SERVICE", msg)
        Log.d("SenSights ENDLESS-SERVICE", msg)
    }

    private fun registerDevice() {
        mSensorManager.registerListener(
                this,
                mAccelerometerSensor,
                SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    private fun stopMeasure() {
        mSensorManager.unregisterListener(this)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        log("accuracy changed")
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event != null) {
            if (event.sensor.type == TYPE_ACCELEROMETER) {
                var loX = event.values[0]
                var loY = event.values[1]
                var loZ = event.values[2]
                saveFirehoseRecord(loX, loY, loZ)
                Handler(Looper.getMainLooper()).postDelayed({
                    stopMeasure()
                }, 10 * 60 * 1000)
            }
        }
    }


    private fun stopService() {
        Toast.makeText(this, "Service stopping", Toast.LENGTH_SHORT).show()
        try {
            wakeLock?.let {
                if (it.isHeld) {
                    it.release()
                }
            }
            stopForeground(true)
            stopSelf()
        } catch (e: Exception) {
        }
        isServiceStarted = false
        Preferences.update(Constants.SERVICE_STATUS, "STOPPED");
    }


    private fun createNotification(): Notification {
        val notificationChannelId = "com.locatemotion.sensights"
        val channelName = "Background Service"
        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(
                notificationChannelId,
                channelName,
                NotificationManager.IMPORTANCE_HIGH
            ).let {
                it.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
                it.lightColor = Color.BLUE
                it.enableVibration(true)
                it
            }
            notificationManager.createNotificationChannel(channel)
        }

        val pendingIntent: PendingIntent =
            Intent(this, MainActivity::class.java).let { notificationIntent ->
                PendingIntent.getActivity(this, 0, notificationIntent, 0)
            }

        val builder: Notification.Builder =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) Notification.Builder(
                this,
                notificationChannelId
            ) else Notification.Builder(this)

        return builder
            .setContentTitle("Sensights app running")
            .setContentIntent(pendingIntent)
            .setTicker("Ticker text")
            .setPriority(Notification.PRIORITY_HIGH) // for under android 26 compatibility
            .build()
    }

    private fun requestHeartRateAndStepCount() {
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        //mHeartRateSensor = mSensorManager.getDefaultSensor(TYPE_HEART_RATE)
        mAccelerometerSensor = mSensorManager.getDefaultSensor(TYPE_ACCELEROMETER)
    }


    /**
     * Initializing Kinesis recorders
     *
     * @see http://docs.aws.amazon.com/ja_jp/mobile/sdkforandroid/developerguide/kinesis.html
     */
    protected fun initRecorder() {
        cognitoIdentityPoolId = getString(R.string.cognito_identity_pool_id)
        region = Regions.fromName(getString(R.string.region))
        androidId = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
        // Get credential from Cognito Identiry Pool
        val directory = applicationContext.cacheDir
        val provider: AWSCredentialsProvider = CognitoCachingCredentialsProvider(
                applicationContext,
                cognitoIdentityPoolId,
                region)
        firehoseRecorder = KinesisFirehoseRecorder(directory, region, provider)
    }


    /**
     * Submit a record to Kinesis Stream
     * @param azimuth
     * @param pitch
     * @param roll
     */
    fun saveFirehoseRecord(loX: Float, loY: Float, loZ: Float) {
        firehoseStreamName = getString(R.string.firehose_stream_name)
        val json = JSONObject()
        try {
            json.accumulate("x", loX)
            json.accumulate("y", loY)
            json.accumulate("z", loZ)
            json.accumulate("email", userEmail)
            json.accumulate("datetime", sdf.format(Date()))
            Log.e(APPLICATION_NAME, json.toString())
            val executor = Executors.newSingleThreadExecutor()
            val handler = Handler(Looper.getMainLooper())
            executor.execute {
                /*
                * Your task will be executed here
                * you can execute anything here that
                * you cannot execute in UI thread
                * for example a network operation
                * This is a background thread and you cannot
                * access view elements here
                *
                * its like doInBackground()
                * */
                try {
                    firehoseRecorder!!.saveRecord(json.toString(), firehoseStreamName)
                    firehoseRecorder!!.submitAllRecords()
                } catch (ace: AmazonClientException) {
                    Log.e(APPLICATION_NAME, "firehose.submitAll failed")
                    //initRecorder();
                }
                handler.post {
                    /*
                    * You can perform any operation that
                    * requires UI Thread here.
                    *
                    * its like onPostExecute()
                    * */
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
}