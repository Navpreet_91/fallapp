package com.example.aws.kinesis.android.mobile_streams.util;

enum class Actions {
    START,
    STOP
}