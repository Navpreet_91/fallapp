package com.example.aws.kinesis.android.mobile_streams.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

	private static final String FILE_NAME_SHARED_PREF = "shared_preference.xml";

	private static Context context;

	private Preferences() { }
	private Preferences(Context context) {
		Preferences.context = context;
	}

	public static void setContext(Context context) {
		Preferences.context = context;
	}

	public static void update(String key, String value) {
		SharedPreferences settings = context.getSharedPreferences(FILE_NAME_SHARED_PREF, Context.MODE_PRIVATE );
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, value);
		editor.commit();

	}

	public static String getString(String key) {
		SharedPreferences settings = context.getSharedPreferences(FILE_NAME_SHARED_PREF,  Context.MODE_PRIVATE);
		return settings.getString(key, "");

	}

	public static String getString(String key, String defaultValue) {
		SharedPreferences settings = context.getSharedPreferences(FILE_NAME_SHARED_PREF,  Context.MODE_PRIVATE);
		return settings.getString(key, defaultValue);

	}

}

