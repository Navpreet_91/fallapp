import { createStackNavigator } from "react-navigation-stack";

import HomeScreen from "../screens/HomeScreen";
import BaselineScreen from "../screens/BaselineScreen";
import MeterScreen from "../screens/MeterScreen";
import InfoScreen from "../screens/InfoScreen";
import Screen1 from "../screens/ExerciseScreen/screen1";
import Screen2 from "../screens/ExerciseScreen/screen2";
import Screen3 from "../screens/ExerciseScreen/screen3";

const defaultHeaderStyle = {
  headerMode: "none",
};

export const HomeStack = createStackNavigator(
  {
    Info: InfoScreen,
    Home: HomeScreen,
    Base: BaselineScreen,
    Meter: MeterScreen,
    Screen1: Screen1,
    Screen2: Screen2,
    Screen3:Screen3
  },
  { initialRouteName: "Info", ...defaultHeaderStyle }
);
