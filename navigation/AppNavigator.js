import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { HomeStack } from "./MainTabNavigator";

const AppNavigator = (props) => {
  const Layout = createAppContainer(
    createSwitchNavigator(
      {
        App: HomeStack,
      },
      {
        initialRouteName: "App",
      }
    )
  );
  return <Layout />;
};

export default AppNavigator;
