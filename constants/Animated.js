import styled from 'styled-components';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native-animatable';
import { MAIN_THEME_COLOR_1, OFF_GREY, ACCENT_COLOR } from './Colors';
import { DEVICE_WIDTH } from './Layout';

export const StyledAnimatedViewContainer = styled(View)`
  padding-horizontal: ${DEVICE_WIDTH * 0.1};
  background-color: ${MAIN_THEME_COLOR_1};
`;

export const StyledAnimatedViewForm = styled(View)`
  margin-top: 20;
  background-color: ${MAIN_THEME_COLOR_1};
`;
export const StyledAnimatedViewFooter = styled(View)`
  height: 100;
  justify-content: center;
  align-content: center;
`;

export const StyledAnimatedViewWrapper = styled(View)`
  flex: 1;
  justify-content: space-between;
`;

export const StyledAnimatedSeparatorContainer = styled(View)`
  align-items: center;
  flex-direction: row;
  margin-vertical: 20;
`;

export const StyledAnimatedSeparator = styled(View)`
  flex: 1;
  border-width: ${StyleSheet.hairlineWidth};
  height: ${StyleSheet.hairlineWidth};
  border-color: ${ACCENT_COLOR};
`;

export const StyledAnimatedText = styled(Text)`
  color: ${OFF_GREY};
  margin-horizontal: 8;
`;
