/**
 * Device Layout Metrics:
 * Heights + Widths & OS specific values
 * Safe max-height for Android
 */
import { Dimensions, Platform } from 'react-native';

var { width, height } = Dimensions.get('window');
export var IS_ANDROID = Platform.OS === 'android';

export var DEVICE_HEIGHT = IS_ANDROID ? height - 24 : height;
export var DEVICE_WIDTH = width;
export var ANDROID_STATUSBAR = 24;

export var IMAGE_WIDTH = DEVICE_WIDTH * 0.8;

export var IMAGE_HEIGHT = DEVICE_HEIGHT / 3;
export var IMAGE_HEIGHT_SMALL = DEVICE_HEIGHT / 6;

export function RESPONSIVE_DEVICE_HEIGHT() {
  height = Dimensions.get('window').height;
  return IS_ANDROID ? height - 24 : height;
}
export function RESPONSIVE_DEVICE_WIDTH() {
  width = Dimensions.get('window').width;
  return width;
}
