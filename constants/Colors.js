
export const MAIN_THEME_COLOR_1 = "#25BEED"; 
export const MAIN_THEME_COLOR_1_LIGHT = "#90bafc"; 
export const MAIN_THEME_COLOR_2 = "#ffb200"; 
export const MAIN_BACKGROUND_LIGHT = "#f4f4f4"; 
export const ACCENT_COLOR = "#fbb32e";

/**
 * Generic Colors
 */
export const WHITE = "#ffffff";
export const OFF_GREY = "#B5BEC7";


export const LIGHT_TEXT = "#ffffff";
export const DARK_TEXT = "#333333";
