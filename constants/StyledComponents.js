import React from 'react';
import styled from 'styled-components';
import { StyleSheet, Image } from 'react-native';
import { Text, View, Icon } from 'native-base';
import { IMAGE_WIDTH, DEVICE_HEIGHT, DEVICE_WIDTH } from './Layout';
import { DARK_TEXT, LIGHT_TEXT } from './Colors';
import { TouchableHighlight } from 'react-native-gesture-handler';

/**
 * Fonts:
 * See App.js as example to import fonts
 */
export const MAIN_TEXT = 'Roboto';
export const MAIN_TEXT_BOLD = 'Roboto_medium';
export const MAIN_TEXT_ITALIC = 'Roboto_italics';
/**
 * COMMON ELEMENT-COMPONENTS :
 * List of commonly used micro element-components styled for keeping the app consistent
 * Ex. Native-base fonts are to be overwritten using StyledText
 */
export const StyledText = styled(Text)`
  font-family: ${MAIN_TEXT};
`;
export const StyledTextBold = styled(Text)`
  font-family: ${MAIN_TEXT_BOLD};
`;
export const LightStyledText = styled(Text)`
  font-family: ${MAIN_TEXT_ITALIC};
`;
export const StyledTitle = styled(Text)`
  font-size: 20;
  font-weight: bold;
  margin-bottom: 10;
`;

/**
 * Controls Icons inside SideBar and Settings
 */
export const StyledIconWrapper = styled(View)`
  width: 40;
`;

/**
 * Logo:
 * See other Screens for various usage on different background colors
 * Ensure the logo has transparent background
 */
export const darkLogo = require('../assets/logo.png');
export const lightLogo = require('../assets/logo.png');

export const globalStyles = StyleSheet.create({
  logoImg: {
    flex: 1,
    width: IMAGE_WIDTH,
    alignSelf: 'center',
    resizeMode: 'contain',
    marginVertical: 24
  },
  baseDarkTextStyle: {
    color: DARK_TEXT,
    fontFamily: MAIN_TEXT
  },
  baseLightTextStyle: {
    color: LIGHT_TEXT,
    fontFamily: MAIN_TEXT
  }
});
export const emptyScreen = StyleSheet.create({
  emptyNotifView: {
    flex: 1,
    alignItems: 'center',
    marginTop: DEVICE_HEIGHT / 4
  },
  emptyNotifText: {
    color: DARK_TEXT,
    fontSize: 16,
    textAlign: 'center'
  }
});
export const StyledHighlight = props => {
  return (
    <TouchableHighlight
      onPress={props.onPress}
      activeOpacity={0.5}
      underlayColor='white'
      style={{
        padding: 10,
        marginVertical: 10
      }}
    >
      {props.children}
    </TouchableHighlight>
  );
};

export const StyledDownIcon = () => {
  return (
    <Icon
      name='chevron-down'
      type='FontAwesome'
      style={{
        fontSize: 15,
        marginTop: 5,
        marginLeft: 5
      }}
    />
  );
};

export const StyledUpIcon = () => {
  return (
    <Icon
      name='chevron-up'
      type='FontAwesome'
      style={{
        fontSize: 15,
        marginTop: 5,
        marginLeft: 5
      }}
    />
  );
};

export const StyledDrawerCover = styled(Image)`
  width: ${DEVICE_WIDTH / 1.8};
  margin-vertical: 20;
  align-self: center;
  height: ${DEVICE_HEIGHT / 10};
  resize-mode: contain;
`;
