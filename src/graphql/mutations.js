/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createFallRisk = /* GraphQL */ `
  mutation CreateFallRisk(
    $input: CreateFallRiskInput!
    $condition: ModelfallRiskConditionInput
  ) {
    createFallRisk(input: $input, condition: $condition) {
      id
      risk
      date
      createdAt
      updatedAt
    }
  }
`;
export const updateFallRisk = /* GraphQL */ `
  mutation UpdateFallRisk(
    $input: UpdateFallRiskInput!
    $condition: ModelfallRiskConditionInput
  ) {
    updateFallRisk(input: $input, condition: $condition) {
      id
      risk
      date
      createdAt
      updatedAt
    }
  }
`;
export const deleteFallRisk = /* GraphQL */ `
  mutation DeleteFallRisk(
    $input: DeleteFallRiskInput!
    $condition: ModelfallRiskConditionInput
  ) {
    deleteFallRisk(input: $input, condition: $condition) {
      id
      risk
      date
      createdAt
      updatedAt
    }
  }
`;
