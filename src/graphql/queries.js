/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getFallRisk = /* GraphQL */ `
  query GetFallRisk($id: ID!) {
    getFallRisk(id: $id) {
      id
      risk
      date
      createdAt
      updatedAt
    }
  }
`;
export const listFallRisks = /* GraphQL */ `
  query ListFallRisks(
    $filter: ModelfallRiskFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listFallRisks(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        risk
        date
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
