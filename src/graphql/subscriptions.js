/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateFallRisk = /* GraphQL */ `
  subscription OnCreateFallRisk {
    onCreateFallRisk {
      id
      risk
      date
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateFallRisk = /* GraphQL */ `
  subscription OnUpdateFallRisk {
    onUpdateFallRisk {
      id
      risk
      date
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteFallRisk = /* GraphQL */ `
  subscription OnDeleteFallRisk {
    onDeleteFallRisk {
      id
      risk
      date
      createdAt
      updatedAt
    }
  }
`;
